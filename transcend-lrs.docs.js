/**
 * @ngdoc overview
 * @name transcend.lrs
 * @description
 # LRS Module
 The "LRS" module provides a set of reusable components related to dealing with data in an LRS.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-lrs.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-lrs/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-lrs](http://code.tsstools.com/bower-lrs)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.lrs']);
 ```

 ## To Do
 - Complete 100% unit test coverage.
 */
/**
 * @ngdoc object
 * @name transcend.esri.lrsConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.lrs LRS Module}. The "lrsConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'esriConfig' object.
 angular.module('myApp').value('lrsConfig', {
     lrsProfile: {
       url: 'http://some-other-service/lrs'
     }
   });
 </pre>
 **/
/**
 * @ngdoc service
 * @name transcend.lrs.$networkLayer
 *
 * @description
 * Provides a set of utility functions related to a plain network layer object (not an instantiated object).
 *
 * The main purpose of this service is to provide common functionality that will be performed on a network layer object.
 *
 <h3>Calling Static Functions</h3>
 <pre>
 $networkLayer.someMethod(layerReference, args);
 </pre>

 <h3>Instantiating</h3>
 <pre>
 // The 'network layer' reference is stored - no need to pass it.
 var lyr = $networkLayer(layerReference);
 lyr.someMethod(args);
 lyr.someOtherMethod(otherArgs);
 </pre>
 *
 * @param {object=} networkLayer The network layer object to 'cache' to perform operations against.
 *
 * @requires transcend.core.$protoFactory
 * @requires $array
 */
/**
       * @ngdoc method
       * @name transcend.lrs.$networkLayer#hierarchyFilter
       * @methodOf transcend.lrs.$networkLayer
       *
       * @description
       * Creates a "hierarchy filter" configuration based on a network layer's route id fields.
       *
       * @param {object=} networkLayer An object with a 'routeIdFields' property on it and a 'field' property.
       * @returns {object} An array of "filter" objects to pass to a hierarchy filter component.
       */
/**
 * @ngdoc directive
 * @name transcend.lrs.directive:lrsProfileDetails
 *
 * @description
 The 'lrsProfileDetails' directive provides an editable template for LRS Profile details.
 *
 * @restrict EA
 * @scope
 *
 * @requires lrsConfig
 *
 */
/**
 * @ngdoc directive
 * @name transcend.lrs.directive:lrsProfileList
 *
 * @description
 The 'lrsProfileList' directive provides an editable template for a Profile list where you can search and select Lrs
 profiles.
 *
 * @restrict EA
 * @scope
 *
 * @requires $resourceListController
 * @requires lrsProfile
 *
 */
/**
 * @ngdoc directive
 * @name transcend.controls.directive:lrsProfileNavbar
 *
 * @description
 The 'lrsProfileNavbar' provides an editable navigation bar template for the Lrs Profile menu
 *
 * @restrict EA
 * @scope
 *
 *
 */
/**
 * @ngdoc service
 * @name transcend.lrs.service:LrsProfile
 *
 * @description
 * The 'LrsProfile' factory provides configuration parameters and direct passage of a URL.
 *
 * @requires $resource
 * @requires $q
 * @requires transcend.lrs.lrsConfig
 * @requires $object
 */
/**
     * Merge this instance with the layer template. The merge will recursively add properties from the source rather
     * than overwrite them. We need to do this because we are overriding existing settings from arcgis server.
     * We have to specifically not merge null values because the data model includes all properties and they
     * may be set to null just because they have not been set yet.
     * @param profileToMerge
     * @param options
     */
