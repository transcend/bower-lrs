# LRS Module
The "LRS" module provides a set of reusable components related to dealing with data in an LRS.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-lrs.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-lrs/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-lrs](http://code.tsstools.com/bower-lrs)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.lrs']);
```

## To Do
- Complete 100% unit test coverage.